//
//  EXPhotoViewer.h
//  EXPhotoViewerDemo
//
//  Created by Julio Carrettoni on 3/20/14.
//  Copyright (c) 2014 Julio Carrettoni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViFStage : UIViewController <UIScrollViewDelegate>

+ (void) stageViF;
+ (void) showImageFrom:(UIImageView*) image url: (NSString*)url;
+ (void) showImageForData: (NSData*) imgData;
- (void) kill;

@end
