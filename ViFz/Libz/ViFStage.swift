//
//  ViFStage.swift
//  ViFz
//
//  Created by Parsa Amini on 29.12.20.
//

import Foundation
import AVKit
import UIKit

class ViFStage:UIView {
    static var stage:ViFStage!
    
    var HEIGHT:CGFloat!
    var WIDTH:CGFloat!
    var controller:UIViewController!
    
    var dismissBG:UIButton!
    
    var player:AVPlayer!
    var avpController = AVPlayerViewController()
    var playerControls:UIView!
    var playBtn:UIButton!
    
    
    static func shared() -> ViFStage {
        if (stage == nil) {
            stage = ViFStage()
        }
        return stage
    }
    
    func stageViF(id: String, gifData: Data) {
        controller = rootViewController()
        HEIGHT = controller.view.frame.size.height
        WIDTH = controller.view.frame.size.width
        
        self.frame = controller.view.frame
        self.backgroundColor = .init(hue: 0.4, saturation: 0.46, brightness: 0.5, alpha: 0.3)
        controller.view.addSubview(self)
        
        dismissBG = UIButton(frame: self.frame)
        dismissBG.addTarget(self, action: #selector(dismissStage), for: .touchDown)
        self.addSubview(dismissBG)
        
        initPlayerView()
        getViF(id: id, play: { (url) in
            self.player = AVPlayer(url: url)
            self.avpController.player = self.player
            self.addSubview(self.avpController.view)
            self.addSubview(self.playerControls)
            self.player.play()
        },
        gif: gifData)
    }
    
    func initPlayerView() {
        
        avpController = AVPlayerViewController()
        avpController.view.frame = CGRect(x: 0, y: (HEIGHT-(HEIGHT*2/3))/2.0, width: WIDTH, height: HEIGHT*2/3)
        avpController.showsPlaybackControls = false
        playerControls = .init(frame: avpController.view.frame)
        playBtn = UIButton(frame: avpController.view.frame)
        playBtn.setImage(UIImage(systemName: "pause.fill"), for: .normal)
        //playBtn.addTarget(self, action: #selector(dismissStage), .touchDown)
        playerControls.addSubview(playBtn)
    }
    
    func rootViewController() -> UIViewController {
        var controller = UIApplication.shared.keyWindow?.rootViewController
        if controller?.presentedViewController != nil {
            controller = controller?.presentedViewController
        }
        return controller!
    }
    
    @objc func dismissStage() {
        player.pause()
        player = nil
        self.removeFromSuperview()
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
    @objc func playPause() {
        if player.rate != 0 {
            player.pause()
        }else {
            player.play()
        }
    }
}
