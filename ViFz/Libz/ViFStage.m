//
//  ViFStage.m
//  ViFStageDemo
//
//  Created by Julio Carrettoni on 3/20/14.
//  Copyright (c) 2014 Julio Carrettoni. All rights reserved.
//

#import "ViFStage.h"
#import <Photos/Photos.h>
#import <SDWebImage/UIImage+GIF.h>
@import MobileCoreServices;

@interface ViFStage ()

@property (nonatomic, retain) UIScrollView *zoomeableScrollView;
@property (nonatomic, retain) UIImageView *theImageView;
@property (nonatomic, retain) UIView* tempViewContainer;
@property (nonatomic, assign) CGRect originalImageRect, theImageViewOrigin;
@property (nonatomic, retain) UIViewController* controller;
@property (nonatomic, retain) UIViewController* selfController;
@property (nonatomic, retain) UIImageView* originalImage;
@property (nonatomic, retain) UIButton *share, *save, *kopierer;
@property (nonatomic, retain) UIVisualEffectView *blurEffectView;
@property NSString *gifUrl;
@property NSData *gifData;
@property CGPoint touchAnker;
@property BOOL touchMovesImage;

@end

static CGFloat s_backgroundScale = 0.945f;

ViFStage *currentStage;

@implementation ViFStage

@synthesize share, save, kopierer, gifUrl, gifData, touchAnker, touchMovesImage;

+ (void) showImageFrom:(UIImageView*) imageView url:(NSString*)url {
    if (currentStage) {
        [currentStage kill];
    }
    if (imageView.image) {
        ViFStage* viewer = [ViFStage new];
        currentStage = viewer;
        viewer.gifUrl = url;
        [viewer showImageFrom:imageView];
    }
}

+ (void) showImageForData:(NSData *)imgData {
    if (currentStage) {
        [currentStage kill];
    }
    if (imgData) {
        ViFStage* stage = [ViFStage new];
        currentStage = stage;
        stage.gifData = imgData;
        [stage showImageFrom:nil];
    }
}

-(void)loadView {
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view.backgroundColor = [UIColor clearColor];
    
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.maximumZoomScale = 10.f;
    scrollView.minimumZoomScale = 1.f;
    scrollView.delegate = self;
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollView.showsVerticalScrollIndicator = FALSE;
    scrollView.showsHorizontalScrollIndicator = FALSE;
    [self.view addSubview: scrollView];
    self.zoomeableScrollView = scrollView;
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [self.zoomeableScrollView addSubview: imageView];
    self.theImageView = imageView;
    
    float imgH = 114.*0.4, imgHc = 126.*0.4;
    float aW = 117*0.4, bW = 91.*0.4, cW = 104.*0.4;
    
    save = [[UIButton alloc] initWithFrame:CGRectMake(self.view.bounds.size.width*2./9., self.view.bounds.size.height-24.-imgH/2.-50., aW, imgH)];
    [save setImage:[UIImage imageNamed:@"save"] forState:UIControlStateNormal];
    [save setContentMode:UIViewContentModeScaleAspectFill];
    [save addTarget:self action:@selector(saveA) forControlEvents:UIControlEventTouchUpInside];
    save.alpha = 0;
    share = [[UIButton alloc] initWithFrame:CGRectMake(self.view.bounds.size.width*4./9., self.view.bounds.size.height-24.-imgH/2.-50., bW, imgH)];
    [share setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
    [share setContentMode:UIViewContentModeScaleAspectFill];
    [share addTarget:self action:@selector(shareA) forControlEvents:UIControlEventTouchUpInside];
    share.alpha = 0;
    kopierer = [[UIButton alloc] initWithFrame:CGRectMake(self.view.bounds.size.width*6./9., self.view.bounds.size.height-24.-imgHc/2.-50., cW, imgHc)];
    [kopierer setImage:[UIImage imageNamed:@"copy"] forState:UIControlStateNormal];
    [kopierer setContentMode:UIViewContentModeScaleAspectFill];
    [kopierer addTarget:self action:@selector(kopier) forControlEvents:UIControlEventTouchUpInside];
    kopierer.alpha = 0;
    
    [self.view addSubview: save];
    [self.view addSubview: share];
    [self.view addSubview: kopierer];
    
    
    UISwipeGestureRecognizer *gR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onBackgroundTap)];
    gR.direction = UISwipeGestureRecognizerDirectionUp | UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:gR];
}

- (void)shareA {
    NSArray *activityItems;
    if (gifData) {
        activityItems = @[gifData];
    }else
        activityItems = @[[NSData dataWithContentsOfURL:[NSURL URLWithString:gifUrl]]];
    
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[UIActivityTypeCopyToPasteboard, UIActivityTypeSaveToCameraRoll];
    
    //for iPhone
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [activityViewControntroller popoverPresentationController].sourceView = share;
    }
    
    [self presentViewController:activityViewControntroller animated:YES completion:nil];
}

- (void)saveA {
    NSData *data;
    if (gifData) {
        data = gifData;
    }else
        data = [NSData dataWithContentsOfURL:[NSURL URLWithString:gifUrl]];
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetResourceCreationOptions *options = [[PHAssetResourceCreationOptions alloc] init];
        [[PHAssetCreationRequest creationRequestForAsset] addResourceWithType:PHAssetResourceTypePhoto data:data options:options];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                    [self actionMessage:@"Saved to Photos"];
            });
            NSLog(@"successfully saved");
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                    [self actionMessage:@"Error Saving"];
            });
            NSLog(@"error saving to photos: %@", error);
        }
    }];
}

- (void)kopier {
    NSData *data;
    if (gifData) {
        data = gifData;
    }else
        data = [NSData dataWithContentsOfURL:[NSURL URLWithString:gifUrl]];
    UIPasteboard *pasteBoard=[UIPasteboard generalPasteboard];
    [pasteBoard setData:data forPasteboardType:(__bridge NSString *)kUTTypeGIF];
    
    [self actionMessage:@"Copied"];
}

-(void)actionMessage: (NSString*) text {
    UILabel *message = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.-100., self.view.frame.size.height/2.-25., 200., 50.)];
    message.layer.cornerRadius = 4.;
    message.layer.masksToBounds = TRUE;
    message.backgroundColor = [UIColor colorWithWhite:0. alpha:.9];
    
    message.textColor = [UIColor whiteColor];
    message.textAlignment = NSTextAlignmentCenter;
    message.adjustsFontSizeToFitWidth = true;
    
    message.text = text;
    
    message.alpha = 0.;
    [self.view addSubview:message];
    [UIView animateWithDuration:.1 animations:^{
        message.alpha = .6;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.12 delay:1.2 options:UIViewAnimationOptionCurveLinear animations:^{
            message.alpha = 0.;
        } completion:^(BOOL finished) {
            [message removeFromSuperview];
        }];
    }];
}

-(UIViewController *) rootViewController{
    UIViewController* controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    if ([controller presentedViewController]) {
        controller = [controller presentedViewController];
    }
    return controller;
}

- (void) showImageFrom:(UIImageView*) imageView {
    UIViewController * controller = [self rootViewController];
    
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleSystemUltraThinMaterialDark];
        self.blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //always fill the view
        self.blurEffectView.frame = controller.view.bounds;

        [controller.view addSubview:self.blurEffectView]; //if you have more UIViews, use an insertSubview API to place it where needed
    }
    
    self.tempViewContainer = [[UIView alloc] initWithFrame:controller.view.bounds];
    self.tempViewContainer.backgroundColor = controller.view.backgroundColor;
    controller.view.backgroundColor = [UIColor blackColor];
    
    for (UIView* subView in controller.view.subviews) {
        [self.tempViewContainer addSubview:subView];
    }
    
    [controller.view addSubview:self.tempViewContainer];
    
    self.controller = controller;
    
    self.view.frame = controller.view.bounds; //CGRectZero;
    self.view.backgroundColor = [UIColor clearColor];
    
    [controller.view addSubview:self.view];

    if (gifData) {
        self.theImageView.image = [UIImage sd_imageWithGIFData:gifData];
        self.originalImageRect = CGRectMake(12.,
                                            self.view.frame.size.height/2. - (self.view.frame.size.width-24.)/2.,
                                            self.view.frame.size.width-24.,
                                            self.view.frame.size.width-24.);

        self.theImageView.frame = self.originalImageRect;
    } else {
        self.theImageView.image = imageView.image;
        self.originalImageRect = [imageView convertRect:imageView.bounds toView:self.view];

        self.theImageView.frame = self.originalImageRect;
    }
    
    //listen to the orientation change notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];

    [UIView animateWithDuration:.1 animations:^{
        self->save.alpha=0.8;
        self->share.alpha=0.8;
        self->kopierer.alpha=0.8;
    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        if (UIAccessibilityIsReduceTransparencyEnabled())
            self.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7];
        else
            self.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
        
        self.tempViewContainer.layer.transform = CATransform3DMakeScale(s_backgroundScale, s_backgroundScale, s_backgroundScale);
        self.theImageView.frame = [self centeredOnScreenImage:self.theImageView.image];
    } completion:^(BOOL finished) {
        [self adjustScrollInsetsToCenterImage];
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBackgroundTap)];
        [self.view addGestureRecognizer:tap];
    }];
    self.theImageViewOrigin = self.theImageView.frame;
    self.selfController = self; //Stupid ARC I need to do this to avoid being dealloced :P
    self.originalImage = imageView;
    if (imageView) {
        imageView.image = nil;
    }
}

-(void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)orientationDidChange:(NSNotification *)note
{
    self.theImageView.frame = [self centeredOnScreenImage:self.theImageView.image];

    CGRect newFrame = [self rootViewController].view.bounds;
    self.tempViewContainer.frame = newFrame;
    self.view.frame = newFrame;
    [self adjustScrollInsetsToCenterImage];

}

- (void) onBackgroundTap {
    if (self.zoomeableScrollView.zoomScale > 1.) {
        return;
    }
        
        
    CGRect absoluteCGRect = [self.view convertRect:self.theImageView.frame fromView:self.theImageView.superview];
    self.zoomeableScrollView.contentOffset = CGPointZero;
    self.zoomeableScrollView.contentInset = UIEdgeInsetsZero;
    self.theImageView.frame = absoluteCGRect;
    
    CGRect originalImageRect;
    if (self.originalImage) {
        originalImageRect = [self.originalImage convertRect:self.originalImage.frame toView:self.view];
    } else {
        originalImageRect = CGRectMake(12.,
                                       self.view.frame.size.height/2. - (self.view.frame.size.width-24.)/2.,
                                       self.view.frame.size.width-24.,
                                       self.view.frame.size.width-24.);
    }
    //originalImageRect is now scaled down, need to adjust
    CGFloat scaleBack = 1.0/s_backgroundScale;
    CGFloat x = originalImageRect.origin.x;
    CGFloat y = originalImageRect.origin.y;
    CGFloat maxX = self.view.frame.size.width;
    CGFloat maxY = self.view.frame.size.height;
    
    y = (y - (maxY / 2.0) ) * scaleBack + (maxY / 2.0);
    x= (x - (maxX / 2.0) ) * scaleBack + (maxX / 2.0);
    originalImageRect.origin.x = x-2.5;
    originalImageRect.origin.y = y+23.5;
    
    originalImageRect.size.width *= 1.0/s_backgroundScale;
    originalImageRect.size.height *= 1.0/s_backgroundScale;
    //done scaling
    
    if (self.blurEffectView)
        [self.blurEffectView removeFromSuperview];

    [UIView animateWithDuration:.16 animations:^{
        self->save.alpha=0;
        self->share.alpha=0;
        self->kopierer.alpha=0;
        self.view.backgroundColor = [UIColor clearColor];
        self.tempViewContainer.layer.transform = CATransform3DIdentity;
    }];
    
    [UIView animateWithDuration:.3 animations:^{
        if (self.originalImage) {
            self.theImageView.frame = originalImageRect;
        }else {
            self.theImageView.alpha = 0;
        }
    }completion:^(BOOL finished) {
        if (self.originalImage) {
            self.originalImage.image = self.theImageView.image;
        }
        self.controller.view.backgroundColor = self.tempViewContainer.backgroundColor;
        for (UIView* subView in self.tempViewContainer.subviews) {
            [self.controller.view addSubview:subView];
        }
        [self.view removeFromSuperview];
        [self.tempViewContainer removeFromSuperview];
    }];
    
    currentStage = nil;
    self.selfController = nil;//Ok ARC you can kill me now.
}

- (void)kill {
    CGRect absoluteCGRect = [self.view convertRect:self.theImageView.frame fromView:self.theImageView.superview];
    self.zoomeableScrollView.contentOffset = CGPointZero;
    self.zoomeableScrollView.contentInset = UIEdgeInsetsZero;
    self.theImageView.frame = absoluteCGRect;
    
    CGRect originalImageRect;
    if (self.originalImage) {
        originalImageRect = [self.originalImage convertRect:self.originalImage.frame toView:self.view];
    } else {
        originalImageRect = CGRectMake(12.,
                                       self.view.frame.size.height/2. - (self.view.frame.size.width-24.)/2.,
                                       self.view.frame.size.width-24.,
                                       self.view.frame.size.width-24.);
    }
    //originalImageRect is now scaled down, need to adjust
    CGFloat scaleBack = 1.0/s_backgroundScale;
    CGFloat x = originalImageRect.origin.x;
    CGFloat y = originalImageRect.origin.y;
    CGFloat maxX = self.view.frame.size.width;
    CGFloat maxY = self.view.frame.size.height;
    
    y = (y - (maxY / 2.0) ) * scaleBack + (maxY / 2.0);
    x= (x - (maxX / 2.0) ) * scaleBack + (maxX / 2.0);
    originalImageRect.origin.x = x-2.5;
    originalImageRect.origin.y = y+23.5;
    
    originalImageRect.size.width *= 1.0/s_backgroundScale;
    originalImageRect.size.height *= 1.0/s_backgroundScale;
    if (self.blurEffectView)
        [self.blurEffectView removeFromSuperview];
    if (self.originalImage) {
        self.originalImage.image = self.theImageView.image;
        self.theImageView.frame = originalImageRect;
    }
    self.tempViewContainer.layer.transform = CATransform3DIdentity;
    
    
    self.controller.view.backgroundColor = self.tempViewContainer.backgroundColor;
    for (UIView* subView in self.tempViewContainer.subviews) {
        [self.controller.view addSubview:subView];
    }
    [self.view removeFromSuperview];
    [self.tempViewContainer removeFromSuperview];
    self.selfController = nil;//Ok ARC you can kill me now.
}

- (CGRect) centeredOnScreenImage:(UIImage*) image {
    CGSize imageSize = [self imageSizesizeThatFitsForImage:self.theImageView.image];
    CGPoint imageOrigin = CGPointMake(self.view.frame.size.width/2.0 - imageSize.width/2.0, self.view.frame.size.height/2.0 - imageSize.height/2.0);
    return CGRectMake(imageOrigin.x, imageOrigin.y, imageSize.width, imageSize.height);
}

- (CGSize) imageSizesizeThatFitsForImage:(UIImage*) image {
    if (!image)
        return CGSizeZero;
    
    CGSize imageSize = image.size;
    CGFloat ratio = MIN(self.view.frame.size.width/imageSize.width, self.view.frame.size.height/imageSize.height);
    //ratio = MIN(ratio, 1.0);//If the image is smaller than the screen let's not make it bigger
    return CGSizeMake(imageSize.width*ratio, imageSize.height*ratio);
}

#pragma mark - ZOOM
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.theImageView;
}

- (void) adjustScrollInsetsToCenterImage {
    CGSize imageSize = [self imageSizesizeThatFitsForImage:self.theImageView.image];
    self.zoomeableScrollView.zoomScale = 1.0;
    self.theImageView.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
    self.zoomeableScrollView.contentSize = self.theImageView.frame.size;
    
    CGRect innerFrame = self.theImageView.frame;
    CGRect scrollerBounds = self.zoomeableScrollView.bounds;
    CGPoint myScrollViewOffset = self.zoomeableScrollView.contentOffset;
    
    if ( ( innerFrame.size.width < scrollerBounds.size.width ) || ( innerFrame.size.height < scrollerBounds.size.height ) )
    {
        CGFloat tempx = self.theImageView.center.x - ( scrollerBounds.size.width / 2 );
        CGFloat tempy = self.theImageView.center.y - ( scrollerBounds.size.height / 2 );
        myScrollViewOffset = CGPointMake( tempx, tempy);
    }
    
    UIEdgeInsets anEdgeInset = { 0, 0, 0, 0};
    if ( scrollerBounds.size.width > innerFrame.size.width )
    {
        anEdgeInset.left = (scrollerBounds.size.width - innerFrame.size.width) / 2;
        anEdgeInset.right = -anEdgeInset.left; // I don't know why this needs to be negative, but that's what works
    }
    if ( scrollerBounds.size.height > innerFrame.size.height )
    {
        anEdgeInset.top = (scrollerBounds.size.height - innerFrame.size.height) / 2;
        anEdgeInset.bottom = -anEdgeInset.top; // I don't know why this needs to be negative, but that's what works
    }
    
    self.zoomeableScrollView.contentOffset = myScrollViewOffset;
    self.zoomeableScrollView.contentInset = anEdgeInset;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    UIView* view = self.theImageView;
    
    CGRect innerFrame = view.frame;
    CGRect scrollerBounds = scrollView.bounds;
    CGPoint myScrollViewOffset = scrollView.contentOffset;
    
    if ( ( innerFrame.size.width < scrollerBounds.size.width ) || ( innerFrame.size.height < scrollerBounds.size.height ) )
    {
        CGFloat tempx = view.center.x - ( scrollerBounds.size.width / 2 );
        CGFloat tempy = view.center.y - ( scrollerBounds.size.height / 2 );
        myScrollViewOffset = CGPointMake( tempx, tempy);
    }
    
    UIEdgeInsets anEdgeInset = { 0, 0, 0, 0};
    if ( scrollerBounds.size.width > innerFrame.size.width )
    {
        anEdgeInset.left = (scrollerBounds.size.width - innerFrame.size.width) / 2;
        anEdgeInset.right = -anEdgeInset.left; // I don't know why this needs to be negative, but that's what works
    }
    if ( scrollerBounds.size.height > innerFrame.size.height )
    {
        anEdgeInset.top = (scrollerBounds.size.height - innerFrame.size.height) / 2;
        anEdgeInset.bottom = -anEdgeInset.top; // I don't know why this needs to be negative, but that's what works
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        scrollView.contentOffset = myScrollViewOffset;
        scrollView.contentInset = anEdgeInset;
    }];
}
@end
