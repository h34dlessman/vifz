//
//  ViewController.swift
//  ViFz
//
//  Created by Parsa Amini on 12.12.20.
//

import UIKit
import AVKit
import AVFoundation

import SDWebImage
import NVActivityIndicatorView
import FirebaseUI

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var WIDTH:CGFloat = 0.0
    var HEIGHT:CGFloat = 0.0
    var TOP_INSET:CGFloat = 0.0
    var BOTTOM_INSET:CGFloat = 0.0
    let topHeightDefault:CGFloat = 80.0
    var topHeight:CGFloat = 0.0
    let bottomHeightDefault:CGFloat = 50.0
    var bottomHeight:CGFloat = 0.0
    
    var collView:UICollectionView!
    let topBar = UIView()
    let bottomBar = UIView()
    let reuseId = "ViFCell"
    let imagePickerController = UIImagePickerController()
    var progressIndicator:NVActivityIndicatorView!
    
    
    var uploadFin:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lisherListo = self.vifCreated
        lisherErro = self.lisherErroHandler
        refreshViFz()
        
        WIDTH = self.view.frame.size.width
        HEIGHT = self.view.frame.size.height
        topHeight = topHeightDefault
        bottomHeight = bottomHeightDefault
        
        topBar.frame = CGRect(x: 0, y: 0, width: WIDTH, height: topHeight)
        topBar.backgroundColor = .secondarySystemFill
        self.view.addSubview(topBar)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: WIDTH/2-5, height: WIDTH/2-5)
        //layout.sectionInset = .zero
        //layout.minimumLineSpacing = 0.0
        //layout.minimumInteritemSpacing = 0.0
        
        collView = UICollectionView(frame: CGRect.init(x: 0, y: 0, width: WIDTH, height: HEIGHT-(bottomHeight)), collectionViewLayout: layout)
        collView.contentInset = .init(top: topHeight, left: 0, bottom: 0, right: 0)
        collView.backgroundColor = .clear
        
        collView.register(ViFCell.self, forCellWithReuseIdentifier: reuseId);
        collView.dataSource = self;
        collView.delegate = self;
        collView.alwaysBounceVertical = true;
        
        let refresher:UIRefreshControl = UIRefreshControl()
        refresher.addTarget(self, action: #selector(refreshViFz), for: .valueChanged)
        collView.refreshControl = refresher
        
        //collView.refreshControl = UIRefreshControl(frame: CGRect(x: -10, y: WIDTH/2-5, width: 10, height: 10), primaryAction: )
        self.view.addSubview(collView);
        
        bottomBar.frame = CGRect(x: 0, y: collView.frame.size.height+collView.frame.origin.y, width: WIDTH, height: bottomHeight)
        bottomBar.backgroundColor = .secondarySystemFill
        self.view.addSubview(bottomBar)
                
        
        progressIndicator = NVActivityIndicatorView(frame: CGRect(x: WIDTH/2-50, y: HEIGHT/2-50, width: 100, height: 100), type: .pacman, color: .red, padding: 0)
        self.view.addSubview(progressIndicator)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        TOP_INSET = self.view.safeAreaInsets.top
        BOTTOM_INSET = self.view.safeAreaInsets.bottom
        if BOTTOM_INSET>0 || TOP_INSET>0 {
            topHeight = topHeightDefault+TOP_INSET
            bottomHeight = bottomHeightDefault+BOTTOM_INSET
            
            topBar.frame = CGRect(x: 0, y: 0, width: WIDTH, height: topHeight)
            collView.frame = CGRect.init(x: 0, y: 0, width: WIDTH, height: HEIGHT-(bottomHeight))
            collView.contentInset = .init(top: topHeight-TOP_INSET, left: 0, bottom: 0, right: 0)
            bottomBar.frame = CGRect(x: 0, y: collView.frame.size.height+collView.frame.origin.y, width: WIDTH, height: bottomHeight)
        }
        displayBars()
    }
    
    func displayBars() {
        if topBar.subviews.count==0 {
            let title = UILabel.init(frame: CGRect(x: (WIDTH-150)/2.0, y: TOP_INSET, width: 150.0, height: topHeightDefault))
            title.text = "ViFz"
            title.textAlignment = .center
            title.font = .boldSystemFont(ofSize: 32.0)
            topBar.addSubview(title)
            
            
            let uploadBtn: UIButton = UIButton.init(frame: CGRect.init(x:50.0, y: 15.0, width:(WIDTH-100)/2.0, height: 30))
            uploadBtn.addTarget(self, action: #selector(uploadAct), for: .touchUpInside)
            uploadBtn.setImage(UIImage.init(systemName: "bolt"), for: .normal)
            uploadBtn.setTitleColor(.red, for: .normal)
            bottomBar.addSubview(uploadBtn)
        }
    }
    // ACTIONS
    @IBAction func uploadAct() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { _ in
            self.openPhotoLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = self.view.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        uploadFin = false
        progressIndicator.color = .red
        self.present(alert, animated: true)
        
    }
    
    @IBAction func downloadNPlay(ViF_id: String, gifData: Data) {
        progressIndicator.startAnimating()
        getViF(id: ViF_id, play: { (filename) in
            self.playVideo(url: filename)
            self.progressIndicator.stopAnimating()
        }, gif: gifData)
    }
    
    // MISCELLANIOUS
    func playVideo(url: URL) {
        let player = AVPlayer(url: url)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    
    
    /**
     =====================================================
     COLLECTION VIEW
     ==================================================
     >>>>>>>>
     */
    
    
    func collectionView( _ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ViFzListIDs.count;
    }
    
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as! ViFCell
        let id = ViFzListIDs[indexPath.row]
        
        let gifRef = storageRef?.child("vifz/\(id)GiF")
        DispatchQueue.main.async {
            if gifRef != nil {
                cell.imageView.sd_setImage(with: gifRef!, placeholderImage: nil) { (img, err, cacheType, ref) in
                print(err ?? "")
            }
            }
            
        }
        cell.id=id
        //cell.title.text = (ViFzList[id] as! Dictionary<String, Any>)["title"] as? String
        
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = (collectionView.cellForItem(at: indexPath) as! ViFCell)
        let id = cell.id!
        ViFStage.shared().stageViF(id: id, gifData: (cell.imageView.image?.sd_imageData())!)
        //downloadNPlay(ViF_id: id)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > -1*topHeight && scrollView.contentOffset.y < -1*topHeight/3.0 {
            let d = (scrollView.contentOffset.y+topHeight)/(topHeight/3.0)
            topBar.alpha = 1-d
            let scale = 1.0-(d*0.05)
            topBar.layer.transform = CATransform3DMakeScale(1.0, scale, scale)
        }
    }
    
    /**     <<<<<<<<<<
     ==================================================
     COLLECTIO VIEW
     ==================================================
     */
    
    @objc func refreshViFz() {
        getViFzList {
            self.collView.reloadData()
            self.collView.refreshControl?.endRefreshing()
        }
    }
    
    
    
    /*
     
     DACKU
     
     */
    func openCamera() {
        imagePickerController.sourceType = .camera
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = ["public.movie"]
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func openPhotoLibrary() {
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = ["public.movie"]
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        progressIndicator.startAnimating()
        
        imagePickerController.dismiss(animated: true, completion: nil)
        
        let filePath = info[UIImagePickerController.InfoKey.mediaURL] as? URL
        
        //Upload Video
        do {
            #warning("TITUL AND TAGS AUTOFILLED")
            try uploadViF(filePath!, title: "ilTitulHia", tags: ["tag1","tag","fill"])
        } catch let err {
            UIAlertController(title: "error", message: errH(err), preferredStyle: .alert)
        }
    
    }
    
    func vifCreated(_ id: String) {
            self.progressIndicator.color = .orange
            if self.uploadFin {
                self.progressIndicator.stopAnimating()
                print("data upload successfull")
                self.refreshViFz()
            }
            self.uploadFin = true
    }
    
    func lisherErroHandler(_ err: Any) {
        UIAlertController(title: "error", message: errH(err as! Error), preferredStyle: .alert)
    }
}
