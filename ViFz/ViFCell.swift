//
//  ViFCell.swift
//  ViFz
//
//  Created by Parsa Amini on 13.12.20.
//

import Foundation
import SDWebImage

class ViFCell: UICollectionViewCell {
    var imageView:SDAnimatedImageView!
    var title:UILabel!
    var id:String!
    var delegate:Any?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //title = UILabel(frame: CGRect(x: 0, y: frame.size.height-20.0, width: frame.size.width, height: 20.0))
        //self.addSubview(title);
        //self.backgroundColor = .black
        imageView = SDAnimatedImageView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageView.layer.masksToBounds=true
        
        self.addSubview(imageView)
        imageView.contentMode = .scaleAspectFill
        //title.textAlignment = .center
        //title.adjustsFontSizeToFitWidth = true
        imageView.backgroundColor = .white
        //title.backgroundColor = .white
        //title.textColor = .black
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
