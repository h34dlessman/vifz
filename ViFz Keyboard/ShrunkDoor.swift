//
//  Generator.swift
//  ViFz
//
//  Created by Parsa Amini on 25.12.20.
//

import Foundation

import Firebase
import FirebaseFirestore
import FirebaseUI


let db = Firestore.firestore()
let storage = Storage.storage()
var storageRef:StorageReference? = nil

var ViFzList = [String: Any]()
var ViFzListIDs = [String]()

func initializeDoor() {
    storageRef = storage.reference()
    getViFzList {}
}


// LOADER
func getViFzList(finsh: @escaping ()->()) {
    
    db.collection("vifz").order(by: "created").getDocuments { (querySnapshot, err) in
        if let err = err {
            print("Error getting ViFzList: \(err)")
        } else {
            ViFzList = [:]
            for document in querySnapshot!.documents {
                let id = document.documentID
                ViFzList[id] = document.data()
            }
        }
        ViFzListIDs = Array(ViFzList.keys)
        DispatchQueue.main.async {
            finsh()
        }
    }
}
// Guettas
func getViF(id: String, done: @escaping (URL)->()) {
    let vifRef = storageRef?.child("vifz/\(id).mp4")
    
    vifRef?.downloadURL(completion: { (vifDlUrl, error) in
        if let error = error {
            print(error.localizedDescription)
            done(vifDlUrl!)
        } else {
            print("\n\n\n\nBEFOAAAAAAAAAAAAAAAAAAAAAAAAAAAA: ",UIPasteboard.general.items,"\n\n\n\n")
            
            
            //print("dlUrl",vifDlUrl!.absoluteString," gif-bytes",gif.count)
            UIPasteboard.general.items = [["public.url":vifDlUrl!]]
            done(vifDlUrl!)
            
            print("\n\n\n\nAFTAAAAAAAAAAAAAAAAAAAAAAAAAAAAA: ",UIPasteboard.general.items,"\n\n\n\n")
        }
    })
}
// Guettas
func getViFLocal(id: String, done: @escaping (URL)->()) {
    let vifRef = storageRef?.child("vifz/\(id).mp4")
    let filepath = getDocumentsDirectory().appendingPathComponent("video.mp4")
    
    vifRef?.write(toFile: filepath, completion: { (vifDlUrl, error) in
        if let error = error {
            print(error.localizedDescription)
            done(filepath)
        } else {
            done(filepath)
            
        }
    })
}

// LOWERS
func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
}


