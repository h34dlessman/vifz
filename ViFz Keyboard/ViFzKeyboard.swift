//
//  ViFzKeyboard.swift
//  ViFz
//
//  Created by Parsa Amini on 26.12.20.
//

import UIKit
//
//protocol ViFzKeyboardDelegate: class {
//  func insertCharacter(_ newCharacter: String)
//  func deleteCharacterBeforeCursor()
//  func characterBeforeCursor() -> String?
//}

let CELLS_PER_ROW:CGFloat = 3.0
let CELL_SPACING:CGFloat = 2.5

class ViFzKeyboard: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collView:UICollectionView!
    let reuseId = "ViFCell"
    var cellSize:CGFloat!
    var delegate:Any?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialize()
    }
    
    func initialize() {
        
        let WIDTH = self.frame.size.width-2.0*CELL_SPACING*2.0
        let HEIGHT = self.frame.size.height-2.0*CELL_SPACING*2.0
        
        cellSize = WIDTH/CELLS_PER_ROW - CELL_SPACING
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: cellSize, height: cellSize)
        layout.itemSize = CGSize(width: cellSize, height: cellSize)
        layout.minimumLineSpacing = CELL_SPACING
        layout.minimumInteritemSpacing = CELL_SPACING
        
        collView = UICollectionView(frame: self.frame, collectionViewLayout: layout)
        collView.contentInset = UIEdgeInsets(top: CELL_SPACING*2.0, left: CELL_SPACING*2.0, bottom: 0, right: CELL_SPACING*2.0)
        collView.showsVerticalScrollIndicator = false
        collView.showsHorizontalScrollIndicator = false
        
        collView.dataSource = self
        collView.delegate = self
        collView.register(ViFCell.self, forCellWithReuseIdentifier: reuseId);
        //collView.alwaysBounceVertical = true;
       
        self.addSubview(collView)
        print("HOOO", self.frame)
        
        getViFzList {
            self.collView.reloadData()
            //self.collView.refreshControl?.endRefreshing()
        }
        
    }
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        print("layoutIfNeeded")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        print("layoutSubviews")
    }
    
    func collectionView( _ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Reloading CollView", ViFzListIDs.count)
        return ViFzListIDs.count;
    }
    
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as! ViFCell
        let id = ViFzListIDs[indexPath.row]
        
        let gifRef = storageRef?.child("vifz/\(id)GiF")
        DispatchQueue.main.async {
            if gifRef != nil {
                cell.imageView.sd_setImage(with: gifRef!, placeholderImage: nil) { (img, err, cacheType, ref) in
                print(err ?? "")
            }
            }
        }
        cell.id=id
        cell.backgroundColor = .red
        if (delegate != nil) {
            cell.delegate = delegate as Any
        }
        print("Drawing Cell", id)
        return cell;
    }
    
    // COLL LAYOUT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSize, height: cellSize)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CELL_SPACING
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CELL_SPACING
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
