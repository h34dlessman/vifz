//
//  ViFCell.swift
//  ViFz
//
//  Created by Parsa Amini on 13.12.20.
//

import Foundation
import SDWebImage

let COPY_TEXT_DEFAULT = "ViF copied\n\ncopy GiF?"
let COPY_TEXT_GiF = "GiF copied"

class ViFCell: UICollectionViewCell {
    var imageView:SDAnimatedImageView!
    var title:UILabel!
    var id:String!
    
    var delegate:Any?
    
    var copied:Int!
    let copyShine = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //title = UILabel(frame: CGRect(x: 0, y: frame.size.height-20.0, width: frame.size.width, height: 20.0))
        //self.addSubview(title);
        //self.backgroundColor = .black
        imageView = SDAnimatedImageView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageView.layer.masksToBounds=true
        
        self.addSubview(imageView)
        imageView.contentMode = .scaleAspectFill
        //title.textAlignment = .center
        //title.adjustsFontSizeToFitWidth = true
        imageView.backgroundColor = .white
        //title.backgroundColor = .white
        //title.textColor = .black
        copyShine.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        copyShine.numberOfLines = 0
        copyShine.textAlignment = .center
        copyShine.alpha = 0
        copyShine.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.76)
        copyShine.textColor = .init(white: 1, alpha: 0.9)
        copyShine.text = COPY_TEXT_DEFAULT
        self.addSubview(copyShine)
        
        copied = 0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTap))
        self.addGestureRecognizer(tap)
        
    }
    
    func copyViF() {
        getViF(id: id) { [self] _ in
            copied = 1
            
            glup()
            UIView.animate(withDuration: 0.14) {
                self.copyShine.alpha = 1.0
            }
            
            self.perform(#selector(clearCopyShine), with: nil, afterDelay: 1.6)
        }
        
    }
    
    func copyGiF() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        UIPasteboard.general.addItems([["com.compuserve.gif":imageView.image!.sd_imageData()!]])
        copied = 2
        
        UIView.animate(withDuration: 0.13) {
            self.copyShine.text = COPY_TEXT_GiF
        }
        self.perform(#selector(clearCopyShine), with: nil, afterDelay: 1.6)
    }
    
    @objc func clearCopyShine() {
        UIView.animate(withDuration: 0.2) {
            self.copyShine.alpha = 0.0
        } completion: { (_) in
            self.copyShine.text = COPY_TEXT_DEFAULT
            self.copied = 0
        }
    }
    
    func glup() {
        UIView.animate(withDuration: 0.012) {
            self.transform = CGAffineTransform(scaleX: 0.9886, y: 0.9886)
        } completion: { (_) in
            UIView.animate(withDuration: 0.003, delay: 0.06, options: .curveEaseOut) {
                self.transform = .identity
            } completion: { (_) in }
            
        }
    }
    
    @objc func onTap() {
        
        if copied==0 {
            copyViF()
        }else if copied==1{
            copyGiF()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
